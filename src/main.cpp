
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

int dataPin = D0;
int shiftPin = D1;
int latchPin = D2;

int dataPin2 = D5;
int shiftPin2 = D6;
int latchPin2 = D7;

//Player one
boolean onefirst = false;
boolean onesecound = false;
boolean onethird = false;
boolean onefourth = false;
boolean onefifth = false;
boolean onesixth = false;
boolean oneseventh = false;
boolean oneeighth = false;
boolean oneninth = false;

//Player two
boolean twofirst = false;
boolean twosecound = false;
boolean twothird = false;
boolean twofourth = false;
boolean twofifth = false;
boolean twosixth = false;
boolean twoseventh = false;
boolean twoeighth = false;
boolean twoninth = false;

//WIN
boolean win = false;

//HELLO
boolean hello = false;


int anode_decimal[]={1, 2, 4, 8, 16, 32, 64, 128};
int cathode_decimal[]={85, 170, 85, 170, 85, 170, 85, 170};


int anode_decimal_hello[]={1, 2, 4, 8, 16, 32, 64, 128};
int cathode_decimal_hello[]={0, 0, 231, 231, 231, 231, 0, 0, 255, //Display 'H'
                       0, 0, 36, 36, 36, 36, 36, 36, 36, 36, 255, //Display 'E'
                       0, 0, 63, 63, 63, 63, 63, 63, 255, //Display 'L'
                       0, 0, 63, 63, 63, 63, 63, 63, 255, //Display 'L'
                       0, 0, 60, 60, 60, 60, 60, 0, 0,    //Display 'O'
                       255, 127, 255, 127, 255, 127, 255}; //Display '...'


char ssid[]= "dani";
char pass[] = "daertl16";

WiFiClient wifiClient;


// mqtt configuration
const char* server = "io.adafruit.com";
const char* topic = "ertl97/feeds/2017-pos1ext";
const char* clientName = "com.ertl97.nodemcu._20180104_MQTT_Connection_Read";

PubSubClient client(wifiClient);

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] angle:");
  String data;
  for (int i = 0; i < length; i++) {
      data += (char)payload[i];
  }
  
  if(strcmp(data.c_str(), "HELLO") == 0){   
      hello = true;
  }
  else if(strcmp(data.c_str(), "RESET") == 0){
        onefirst = false;
        onesecound = false;
        onethird = false;
        onefourth = false;
        onefifth = false;
        onesixth = false;
        oneseventh = false;
        oneeighth = false;
        oneninth = false;

        twofirst = false;
        twosecound = false;
        twothird = false;
        twofourth = false;
        twofifth = false;
        twosixth = false;
        twoseventh = false;
        twoeighth = false;
        twoninth = false;

        win = false;
        hello = false;

        digitalWrite(latchPin, LOW);
        shiftOut(dataPin, shiftPin, MSBFIRST, 0);    
        digitalWrite(latchPin, HIGH);
   
        digitalWrite(latchPin2, LOW);
        shiftOut(dataPin2, shiftPin2, MSBFIRST, 255);
        digitalWrite(latchPin2, HIGH);
  }

    if(strcmp(data.c_str(), "WIN") == 0){
        win = true;
    }


    if(strcmp(data.c_str(), "11") == 0 && !twofirst){
        onefirst = true;
    }else if(strcmp(data.c_str(), "21") == 0){
        twofirst = true;
    }


    if(strcmp(data.c_str(), "12") == 0 && !twosecound){
      onesecound = true;
    }else if(strcmp(data.c_str(), "22") == 0){
        twosecound = true;
    }

    if(strcmp(data.c_str(), "13") == 0 && !twothird){
      onethird = true;
    }else if(strcmp(data.c_str(), "23") == 0){
        twothird = true;
    }


    if(strcmp(data.c_str(), "14") == 0 && !twofourth){
        onefourth = true;
    }
    else if(strcmp(data.c_str(), "24") == 0){
        twofourth = true;
    }

    if(strcmp(data.c_str(), "15") == 0 && !twofifth){
        onefifth = true;
    }else if(strcmp(data.c_str(), "25") == 0){
        twofifth = true;
    }

    if(strcmp(data.c_str(), "16") == 0 && !twosixth){
        onesixth = true;
    }else if(strcmp(data.c_str(), "26") == 0){
        twosixth = true;
    }

    if(strcmp(data.c_str(), "17") == 0 && !twoseventh){
        oneseventh = true;
    }else if(strcmp(data.c_str(), "27") == 0){
        twoseventh = true;
    }

    if(strcmp(data.c_str(), "18") == 0 && !twoeighth){
        oneeighth = true;
    }else if(strcmp(data.c_str(), "28") == 0){
        twoeighth = true;
    }

    if(strcmp(data.c_str(), "19") == 0 && !twoninth){
        oneninth = true;
    }else if(strcmp(data.c_str(), "29") == 0){
        twoninth = true;
    }






}



void mqttReConnect() {
    while (!client.connected()) {
        Serial.print("Attempting MQTT connection...");
        // Attempt to connect
        if (client.connect(clientName,"ertl97","7c92d693dc414b0babca71df5b74c030")) {
            Serial.println("connected");
            client.subscribe(topic);
        } else {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            delay(5000);
        }
    }
}

void setup() {
    // put your setup code here, to run once:
    pinMode(latchPin, OUTPUT);
  pinMode(shiftPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  pinMode(latchPin2, OUTPUT);
  pinMode(shiftPin2, OUTPUT);
  pinMode(dataPin2, OUTPUT);

    Serial.begin(9600);

    WiFi.begin(ssid, pass);
    Serial.print("Connecting");

    while (WiFi.status() != WL_CONNECTED){
        delay(500);
        Serial.print(".");
    }
    Serial.println();

    Serial.print("Connected, IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println(WiFi.gatewayIP());


    client.setServer(server, 1883);
    client.setCallback(callback);
    mqttReConnect();
    delay(10);
    
}

void setMatrix(int row, int col){
    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, shiftPin, MSBFIRST, row); 
    digitalWrite(latchPin, HIGH);

 
    digitalWrite(latchPin2, LOW);
    shiftOut(dataPin2, shiftPin2, MSBFIRST, col);
    digitalWrite(latchPin2, HIGH);
      
    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, shiftPin, MSBFIRST, 0);    
    digitalWrite(latchPin, HIGH);
  
    digitalWrite(latchPin2, LOW);
    shiftOut(dataPin2, shiftPin2, MSBFIRST, 255);
    digitalWrite(latchPin2, HIGH);
}


void loop() {
    // put your main code here, to run repeatedly:

    
     while (WiFi.status() != WL_CONNECTED)
	{
		
		delay(500);
		Serial.print(".");
	}
	

    if (!client.connected()) {
        mqttReConnect();
    }
    client.loop();



    if(hello){
        int i=0,j=0,k=0,length_of_cathode_decimal_array_hello;
        int temp;
        for(k=0;k<15;k++) 
        {
            for(j=0;j<8;j++) 
            {
            for(i=0;i<5;i++) 
            {    
                digitalWrite(latchPin, LOW);
                shiftOut(dataPin, shiftPin, MSBFIRST, anode_decimal_hello[j]); 
                digitalWrite(latchPin, HIGH);
            
                digitalWrite(latchPin2, LOW);
                shiftOut(dataPin2, shiftPin2, MSBFIRST, cathode_decimal_hello[j]);
                digitalWrite(latchPin2, HIGH);
            }   

            digitalWrite(latchPin, LOW);
            shiftOut(dataPin, shiftPin, MSBFIRST, 0);    
            digitalWrite(latchPin, HIGH);
        
            digitalWrite(latchPin2, LOW);
                shiftOut(dataPin2, shiftPin2, MSBFIRST, 255);
            digitalWrite(latchPin2, HIGH);   
            }   
        }
        

        length_of_cathode_decimal_array_hello = sizeof(cathode_decimal_hello)/sizeof(cathode_decimal_hello[0]);
        temp=cathode_decimal_hello[0];

        for(i=1;i<length_of_cathode_decimal_array_hello;i++)
        {
            cathode_decimal_hello[i-1]=cathode_decimal_hello[i];
        }  
        
        cathode_decimal[length_of_cathode_decimal_array_hello-1]=temp;

    }


    if(win){

        int i=0,j=0,k=0,length_of_cathode_decimal_array;

        int temp;
        for(k=0;k<25;k++)
        {
            for(j=0;j<8;j++) 
            {
            for(i=0;i<5;i++) 
            {    
                digitalWrite(latchPin, LOW);
                shiftOut(dataPin, shiftPin, MSBFIRST, anode_decimal[j]);    
                digitalWrite(latchPin, HIGH);
            
                digitalWrite(latchPin2, LOW);
                shiftOut(dataPin2, shiftPin2, MSBFIRST, cathode_decimal[j]);  
                digitalWrite(latchPin2, HIGH);
            }   
            
            digitalWrite(latchPin, LOW);
            shiftOut(dataPin, shiftPin, MSBFIRST, 0);   
            digitalWrite(latchPin, HIGH);

        
            digitalWrite(latchPin2, LOW);
            shiftOut(dataPin2, shiftPin2, MSBFIRST, 255);
            digitalWrite(latchPin2, HIGH);   
            }   
        }
        
        length_of_cathode_decimal_array = sizeof(cathode_decimal)/sizeof(cathode_decimal[0]);

        temp=cathode_decimal[0];
        
        for(i=1;i<length_of_cathode_decimal_array;i++)
        {
            cathode_decimal[i-1]=cathode_decimal[i];
        }  
        
        cathode_decimal[length_of_cathode_decimal_array-1]=temp; 

    }


    if(onefirst){
        setMatrix(1,254);
        setMatrix(2,253);
    }else if(twofirst){
        setMatrix(1,253);
        setMatrix(2,254);
    }

     if(onesecound){
        setMatrix(1,247);
        setMatrix(2,239);
    }else if(twosecound){
        setMatrix(1,239);
        setMatrix(2,247);
    }

    if(onethird){
        setMatrix(1,191);
        setMatrix(2,127);
    }else if(twothird){
        setMatrix(1,127);
        setMatrix(2,191);
    }

    if(onefourth){
        setMatrix(8,254);
        setMatrix(16,253);
    }else if(twofourth){
        setMatrix(8,253);
        setMatrix(16,254);
    }

    if(onefifth){
        setMatrix(8,247);
        setMatrix(16,239);
    }else if(twofifth){
        setMatrix(8,239);
        setMatrix(16,247);
    }

    if(onesixth){
        setMatrix(8,191);
        setMatrix(16,127);
    }else if(twosixth){
        setMatrix(8,127);
        setMatrix(16,191);
    }

    if(oneseventh){
        setMatrix(64,254);
        setMatrix(128,253);
    }else if(twoseventh){
        setMatrix(64,253);
        setMatrix(128,254);
    }

    if(oneeighth){
        setMatrix(64,247);
        setMatrix(128,239);
    }else if(twoeighth){
        setMatrix(64,239);
        setMatrix(128,247);
    }

    if(oneninth){
        setMatrix(64,191);
        setMatrix(128,127);
    }else if(twoninth){
        setMatrix(64,127);
        setMatrix(128,191);
    }


    
}
